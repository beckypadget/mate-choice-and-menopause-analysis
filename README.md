# Data analysis
This project runs the data analysis for the '[Mate choice and the menopause](https://gitlab.com/beckypadget/mate-choice-and-menopause?nav_source=navbar)' project.

### Requirements
This project has three requirements, plus it needs the data:
* ggplot2
* aTSA
* beepr (not really essential, but some of the vectors take a while to create so it may be handy)

* data

### Data
Data are saved in the `Data` folder in the `Supplementary materials` folder. This is accessible if the working directory is set to the source file location. Data are split up into two folders: `individual` and `combined`. Each file in the `individual` folder contains data from a single run. Each file in the `combined` folder contains data from multiple runs. These are split again due to how data collection was run, but this won't affect analysis as long as the `requirements.R` file is run in full.

### Set up
To start analysis, you will need to run the `requirements.R` file. You can set the working directory in code here (or via the toolbar, if using RStudio) - it is easiest to set this to the source file directory. Running this file will install the required packages, and load the data in.

Once you have run all of `requirements.R`, you are ready to go.

### Usage
In the `analysis.R` file, you will find sections containing functions, vectors, plots and models. The vectors should be created in the order that they appear (because calculating PrR requires mortality ages to be found first).

The `FUNCTIONS` header contains two functions:
* `mean_final` takes in a data frame and a column number and finds the mean of the final n values for each run. The value for n can be altered by changing the `number` variable. The output is a list of 30 doubles.
* `mean_over_time` also takes in a data frame and a column number, but calculates the mean of each run for each time step. The output is a list of 10,000 doubles.

`VIOLIN PLOTS REQUIREMENTS` contains a `ggproto` object and a function `geom_split_violin`. These allow split violin plots to be created using ggplot2.

##### From then on...
* Any header with `FINAL` deals with the mean end values of the model - the lists created by `mean_final`.
* Any header with `TIME SERIES` uses data from the entire time series - the lists created by `mean_over_time`.
* `VECTORS` sections are where vectors are created.
* `PLOTS` sections are where plots are made (and saved to file if you want).

#### Saving graph files
To save graphs, uncomment the `pdf("graphs/... .pdf")` and `dev.off()` code directly above and below each graph. This overwrites the current file with that name, if one is present, or the name can be changed. Graphs will automatically be saved in the `graphs` directory.

#### Power analysis
The `power_analysis.R` file runs power analyses under various effect sizes and plots them.

### Credit
The code in the `VIOLIN PLOTS REQUIREMENTS` section was found at: https://stackoverflow.com/questions/35717353/split-violin-plot-with-ggplot2