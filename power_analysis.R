library(pwr)
par(mfrow=c(1,1))
power_analysis <- pwr.t2n.test(n1 = 250, n2=250 , d = 0.25, sig.level =0.05)
power_analysis$power
# small effect (0.25) -> 250 (80%)
# moderate effect (0.5) -> 65 (80%)

non_whales_means[5001]
x<- true_female$whales[which(true_female$Time==5000)]
z <- true_none$whales[which(true_female$Time==5000)]
a <- c(x,z)
plot(1:10, x, ylim = c(0,200), pch =20)
plot(true_female$whales[which(true_female$Time==5000)])
sd(a)

(153-101)/102


power_small_effect <- c()
for(i in 2:500)
  {
  power_analysis <- pwr.t2n.test(n1 = i, n2 = i , d = 0.25, sig.level = 0.05)
  power_small_effect <- c(power_small_effect, power_analysis$power)
  }
plot(2:500, power_small_effect, pch = 20, xlab = "Sample size", ylab = "Power")

power_moderate_effect <- c()
for(i in 2:500)
{
  power_analysis <- pwr.t2n.test(n1 = i, n2 = i , d = 0.5, sig.level = 0.05)
  power_moderate_effect <- c(power_moderate_effect, power_analysis$power)
}
points(2:500, power_moderate_effect, pch = 20, col= "grey")
###############
# n=65 (0.80) #
# n=105 (0.95)#
###############

power_my_effect <- c()
for(i in 2:100)
{
  power_analysis <- pwr.t2n.test(n1 = i, n2 = i , d = 0.04, sig.level = 0.05)
  power_my_effect <- c(power_my_effect, power_analysis$power)
}
points(2:500, power_my_effect, pch = 20, xlab = "Sample size", ylab = "Power", col= "cyan3")
power_analysis <- pwr.t2n.test(n1 = 30, n2 = 30, d = -1, sig.level = 0.05)
power_analysis

###############
# n=17 (0.80) #
# n=27 (0.95) #
###############

pdf("graphs/power_analysis.pdf")
plot(2:100, power_my_effect, pch = 20, xlab = "Sample size", ylab = "Power")
# legend(300, 0.25, legend = c("Small effect (0.25)", "Moderate effect (0.5)", "Estimate (-1)"), pch = 20, col = c("black", "grey", "cyan3"))
segments(0, 0.96, 30, 0.96, lty =2)
segments(30, 0, 30, 0.96, lty = 2)
dev.off()

