#### FUNCTIONS ####
## Final values
number = 1000 # number of runs it means final values over
mean_final <- function(data, column)
{
  values <- c()
  for (i in 1:iterations)
  {
    values <- c(values, mean(tail(data[,column][which(data[,11]==i-1)], n = number)))
  }
  return(values)
}

# Means over time
mean_over_time <- function(data, column)
{
  prr <- c()
  for (i in 1:steps)
  {
    prr <- c(prr, mean(data[,column][which(data[,1]==i)]))
  }
  return(prr)
}

#### VIOLIN PLOT REQUIREMENTS ####
# Code from jan-glx on stackexchange https://stackoverflow.com/questions/35717353/split-violin-plot-with-ggplot2
GeomSplitViolin <- ggproto("GeomSplitViolin", GeomViolin, 
                           draw_group = function(self, data, ..., draw_quantiles = NULL) {
                             data <- transform(data, xminv = x - violinwidth * (x - xmin), xmaxv = x + violinwidth * (xmax - x))
                             grp <- data[1, "group"]
                             newdata <- plyr::arrange(transform(data, x = if (grp %% 2 == 1) xminv else xmaxv), if (grp %% 2 == 1) y else -y)
                             newdata <- rbind(newdata[1, ], newdata, newdata[nrow(newdata), ], newdata[1, ])
                             newdata[c(1, nrow(newdata) - 1, nrow(newdata)), "x"] <- round(newdata[1, "x"])
                             
                             if (length(draw_quantiles) > 0 & !scales::zero_range(range(data$y))) {
                               stopifnot(all(draw_quantiles >= 0), all(draw_quantiles <=
                                                                         1))
                               quantiles <- ggplot2:::create_quantile_segment_frame(data, draw_quantiles)
                               aesthetics <- data[rep(1, nrow(quantiles)), setdiff(names(data), c("x", "y")), drop = FALSE]
                               aesthetics$alpha <- rep(1, nrow(quantiles))
                               both <- cbind(quantiles, aesthetics)
                               quantile_grob <- GeomPath$draw_panel(both, ...)
                               ggplot2:::ggname("geom_split_violin", grid::grobTree(GeomPolygon$draw_panel(newdata, ...), quantile_grob))
                             }
                             else {
                               ggplot2:::ggname("geom_split_violin", GeomPolygon$draw_panel(newdata, ...))
                             }
                           })

geom_split_violin <- function(mapping = NULL, data = NULL, stat = "ydensity", position = "identity", ..., 
                              draw_quantiles = NULL, trim = TRUE, scale = "area", na.rm = FALSE, 
                              show.legend = NA, inherit.aes = TRUE) {
  layer(data = data, mapping = mapping, stat = stat, geom = GeomSplitViolin, 
        position = position, show.legend = show.legend, inherit.aes = inherit.aes, 
        params = list(trim = trim, scale = scale, draw_quantiles = draw_quantiles, na.rm = na.rm, ...))
}



#### FINAL: VECTORS - PROPORTION MENOPAUSAL #### 
# Total whales
final_whales_fn <- mean_final(false_none, 10)
final_whales_fm <- mean_final(false_male, 10)
final_whales_ff <- mean_final(false_female, 10)
final_whales_tn <- mean_final(true_none, 10)
final_whales_tm <- mean_final(true_male, 10)
final_whales_tf <- mean_final(true_female, 10)

fn_whales_df <- data.frame(false_label, none_label, final_whales_fn)
colnames(fn_whales_df) <- c("GM", "MC", "Values")

tn_whales_df <- data.frame(true_label, none_label, final_whales_tn)
colnames(tn_whales_df) <- c("GM", "MC", "Values")

fm_whales_df <- data.frame(false_label, male_label, final_whales_fm)
colnames(fm_whales_df) <- c("GM", "MC", "Values")

tm_whales_df <- data.frame(true_label, male_label, final_whales_tm)
colnames(tm_whales_df) <- c("GM", "MC", "Values")

ff_whales_df <- data.frame(false_label, female_label, final_whales_ff)
colnames(ff_whales_df) <- c("GM", "MC", "Values")

tf_whales_df <- data.frame(true_label, female_label, final_whales_tf)
colnames(tf_whales_df) <- c("GM", "MC", "Values")

# female whales
# final_sexratio_fn <- mean_final(false_none, 9)
# final_sexratio_fm <- mean_final(false_male, 9)
# final_sexratio_ff <- mean_final(false_female, 9)
# final_sexratio_tn <- mean_final(true_none, 9)
# final_sexratio_tm <- mean_final(true_male, 9)
# final_sexratio_tf <- mean_final(true_female, 9)
# final_females_fn <- final_whales_fn * final_sexratio_fn
# final_females_fm <- final_whales_fm * final_sexratio_fm
# final_females_ff <- final_whales_ff * final_sexratio_ff
# final_females_tn <- final_whales_tn * final_sexratio_tn
# final_females_tm <- final_whales_tm * final_sexratio_tm
# final_females_tf <- final_whales_tf * final_sexratio_tf

# menopausal whales
final_menopausal_fn <- mean_final(false_none, 7)
final_menopausal_fm <- mean_final(false_male, 7)
final_menopausal_ff <- mean_final(false_female, 7)
final_menopausal_tn <- mean_final(true_none, 7)
final_menopausal_tm <- mean_final(true_male, 7)
final_menopausal_tf <- mean_final(true_female, 7)

final_proportion_fn <- final_menopausal_fn / final_whales_fn
final_proportion_fm <- final_menopausal_fm / final_whales_fm
final_proportion_ff <- final_menopausal_ff / final_whales_ff

final_proportion_tn <- final_menopausal_tn / final_whales_tn
final_proportion_tm <- final_menopausal_tm / final_whales_tm
final_proportion_tf <- final_menopausal_tf / final_whales_tf

# final_proportion_tn <- final_proportion_tn[!is.na(final_proportion_tn)]
# final_proportion_tm <- final_proportion_tm[!is.na(final_proportion_tm)]
# final_proportion_tf <- final_proportion_tf[!is.na(final_proportion_tf)]
# 
# final_proportion_fn <- final_proportion_fn[!is.na(final_proportion_fn)]
# final_proportion_fm <- final_proportion_fm[!is.na(final_proportion_fm)]
# final_proportion_ff <- final_proportion_ff[!is.na(final_proportion_ff)]


proportions <- c(final_proportion_fn, final_proportion_tn, 
                 final_proportion_fm, final_proportion_tm,
                 final_proportion_ff, final_proportion_tf)

false_label <- rep("False", times=iterations)
true_label <- rep("True", times=iterations)
none_label <- rep("None", times=iterations)
male_label <- rep("Male", times=iterations)
female_label <- rep("Female", times=iterations)

fn_proportion_df <- data.frame(false_label, none_label, final_proportion_fn)
colnames(fn_proportion_df) <- c("GM", "MC", "Values")

tn_proportion_df <- data.frame(true_label, none_label, final_proportion_tn)
colnames(tn_proportion_df) <- c("GM", "MC", "Values")

fm_proportion_df <- data.frame(false_label, male_label, final_proportion_fm)
colnames(fm_proportion_df) <- c("GM", "MC", "Values")

tm_proportion_df <- data.frame(true_label, male_label, final_proportion_tm)
colnames(tm_proportion_df) <- c("GM", "MC", "Values")

ff_proportion_df <- data.frame(false_label, female_label, final_proportion_ff)
colnames(ff_proportion_df) <- c("GM", "MC", "Values")

tf_proportion_df <- data.frame(true_label, female_label, final_proportion_tf)
colnames(tf_proportion_df) <- c("GM", "MC", "Values")

#### FINAL: VECTORS - MORTALITY AGE ####
head(false_none)
final_deathage_fn <- mean_final(false_none, 4)
final_deathage_fm <- mean_final(false_male, 4)
final_deathage_ff <- mean_final(false_female, 4)

final_deathage_tn <- mean_final(true_none, 4)
final_deathage_tm <- mean_final(true_male, 4)
final_deathage_tf <- mean_final(true_female, 4)

# final_deathage_tn <- final_deathage_tn[!is.na(final_deathage_tn)]
# final_deathage_tm <- final_deathage_tm[!is.na(final_deathage_tm)]
# final_deathage_tf <- final_deathage_tf[!is.na(final_deathage_tf)]
# 
# final_deathage_fn <- final_deathage_fn[!is.na(final_deathage_fn)]
# final_deathage_fm <- final_deathage_fm[!is.na(final_deathage_fm)]
# final_deathage_ff <- final_deathage_ff[!is.na(final_deathage_ff)]

mean(final_deathage_tf)

false_label <- rep("False", times=iterations)
true_label <- rep("True", times=iterations)
none_label <- rep("None", times=iterations)
male_label <- rep("Male", times=iterations)
female_label <- rep("Female", times=iterations)

fn_deathage_df <- data.frame(false_label, none_label, final_deathage_fn)
colnames(fn_deathage_df) <- c("GM", "MC", "Values")

tn_deathage_df <- data.frame(true_label, none_label, final_deathage_tn)
colnames(tn_deathage_df) <- c("GM", "MC", "Values")

fm_deathage_df <- data.frame(false_label, male_label, final_deathage_fm)
colnames(fm_deathage_df) <- c("GM", "MC", "Values")

tm_deathage_df <- data.frame(true_label, male_label, final_deathage_tm)
colnames(tm_deathage_df) <- c("GM", "MC", "Values")

ff_deathage_df <- data.frame(false_label, female_label, final_deathage_ff)
colnames(ff_deathage_df) <- c("GM", "MC", "Values")

tf_deathage_df <- data.frame(true_label, female_label, final_deathage_tf)
colnames(tf_deathage_df) <- c("GM", "MC", "Values")

#### FINAL: VECTORS - PRR ####
fn_prr <- (fn_deathage_df$Values - 40) / (fn_deathage_df$Values - 10)
fm_prr <- (fm_deathage_df$Values - 40) / (fm_deathage_df$Values - 10)
ff_prr <- (ff_deathage_df$Values - 40) / (ff_deathage_df$Values - 10)

tn_prr <- (tn_deathage_df$Values - 40) / (tn_deathage_df$Values - 10)
tm_prr <- (tm_deathage_df$Values - 40) / (tm_deathage_df$Values - 10)
tf_prr <- (tf_deathage_df$Values - 40) / (tf_deathage_df$Values - 10)

fn_prr <- data.frame(false_label, none_label, fn_prr)
colnames(fn_prr) <- c("GM", "MC", "Values")

fm_prr <- data.frame(false_label, male_label, fm_prr)
colnames(fm_prr) <- c("GM", "MC", "Values")

ff_prr <- data.frame(false_label, female_label, ff_prr)
colnames(ff_prr) <- c("GM", "MC", "Values")

tn_prr <- data.frame(true_label, none_label, tn_prr)
colnames(tn_prr) <- c("GM", "MC", "Values")

tm_prr <- data.frame(true_label, male_label, tm_prr)
colnames(tm_prr) <- c("GM", "MC", "Values")

tf_prr <- data.frame(true_label, female_label, tf_prr)
colnames(tf_prr) <- c("GM", "MC", "Values")

#### FINAL: VECTORS - MALE MORTALITY AGE ####
male_deathage_fn <- mean_final(false_none, 6)
male_deathage_fm <- mean_final(false_male, 6)
male_deathage_ff <- mean_final(false_female, 6)

male_deathage_tn <- mean_final(true_none, 6)
male_deathage_tm <- mean_final(true_male, 6)
male_deathage_tf <- mean_final(true_female, 6)

male_deathage_tn <- male_deathage_tn[!is.na(male_deathage_tn)]
male_deathage_tm <- male_deathage_tm[!is.na(male_deathage_tm)]
male_deathage_tf <- male_deathage_tf[!is.na(male_deathage_tf)]

male_deathage_fn <- male_deathage_fn[!is.na(male_deathage_fn)]
male_deathage_fm <- male_deathage_fm[!is.na(male_deathage_fm)]
male_deathage_ff <- male_deathage_ff[!is.na(male_deathage_ff)]
mean(male_deathage_fm)
mean(final_deathage_fm)


#### FINAL: VECTORS - SEX RATIO ####
head(false_none)
# total whales
final_sexratio_fn <- mean_final(false_none, 9)
final_sexratio_fm <- mean_final(false_male, 9)
final_sexratio_ff <- mean_final(false_female, 9)
final_sexratio_tn <- mean_final(true_none, 9)
final_sexratio_tm <- mean_final(true_male, 9)
final_sexratio_tf <- mean_final(true_female, 9)

# final_sexratio_tn <- final_sexratio_tn[!is.na(final_sexratio_tn)]
# final_sexratio_tm <- final_sexratio_tm[!is.na(final_sexratio_tm)]
# final_sexratio_tf <- final_sexratio_tf[!is.na(final_sexratio_tf)]
# 
# final_sexratio_fn <- final_sexratio_fn[!is.na(final_sexratio_fn)]
# final_sexratio_fm <- final_sexratio_fm[!is.na(final_sexratio_fm)]
# final_sexratio_ff <- final_sexratio_ff[!is.na(final_sexratio_ff)]

mean(final_sexratio_tf)

fn_sexratio_df <- data.frame(false_label, none_label, final_sexratio_fn)
colnames(fn_sexratio_df) <- c("GM", "MC", "Values")

tn_sexratio_df <- data.frame(true_label, none_label, final_sexratio_tn)
colnames(tn_sexratio_df) <- c("GM", "MC", "Values")

fm_sexratio_df <- data.frame(false_label, male_label, final_sexratio_fm)
colnames(fm_sexratio_df) <- c("GM", "MC", "Values")

tm_sexratio_df <- data.frame(true_label, male_label, final_sexratio_tm)
colnames(tm_sexratio_df) <- c("GM", "MC", "Values")

ff_sexratio_df <- data.frame(false_label, female_label, final_sexratio_ff)
colnames(ff_sexratio_df) <- c("GM", "MC", "Values")

tf_sexratio_df <- data.frame(true_label, female_label, final_sexratio_tf)
colnames(tf_sexratio_df) <- c("GM", "MC", "Values")




#### FINAL: DATAFRAMES - ALL ####
prrs_new <- rbind(fn_prr, fm_prr, ff_prr, tn_prr, tm_prr, tf_prr)

whales <- rbind(fn_whales_df, tn_whales_df,fm_whales_df, 
                tm_whales_df, ff_whales_df, tf_whales_df)

proportions <- rbind(fn_proportion_df, tn_proportion_df,fm_proportion_df,
                     tm_proportion_df, ff_proportion_df, tf_proportion_df)

deathage <- rbind(fn_deathage_df, tn_deathage_df,fm_deathage_df, tm_deathage_df,
                  ff_deathage_df, tf_deathage_df)

sexratios <- rbind(fn_sexratio_df, tn_sexratio_df,fm_sexratio_df, tm_sexratio_df,
                   ff_sexratio_df, tf_sexratio_df)






#### FINAL: PLOT - PRR ####
# pdf("graphs/violin_prr.pdf")
ggplot(mapping=aes(x=prrs_new$MC, y=prrs_new$Values, fill = prrs_new$GM), data = prrs_new) +
  geom_split_violin() +
  theme_bw() +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  guides(fill=guide_legend(title="Grandmother effect")) +
  theme(axis.title.x = element_blank()) +   # Remove x-axis label
  ylab("Post-reproductive representation") +
  scale_fill_manual(values=c("darkcyan", "cyan3"))
# dev.off()

#### FINAL: PLOT - PROPORTION OF MENOPAUSAL WHALES ####
pdf("graphs/violin_proportion.pdf")
ggplot(mapping=aes(x=proportions$MC, y=proportions$Values, fill = proportions$GM), data = proportions) +
  geom_split_violin() +
  theme_bw() +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  guides(fill=guide_legend(title="Grandmother effect")) +
  theme(axis.title.x = element_blank()) +   # Remove x-axis label
  ylab("Proportion post-reproductive") +
  scale_fill_manual(values=c("darkcyan", "cyan3"))
dev.off()


#### FINAL: PLOT - MORTALITY AGE ####
# pdf("graphs/violin_mortality.pdf")
ggplot(mapping=aes(x=deathage$MC, y=deathage$Values, fill = deathage$GM), data = deathage) +
  geom_split_violin() +
  theme_bw() +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  guides(fill=guide_legend(title="Grandmother effect")) +
  theme(axis.title.x = element_blank()) +   # Remove x-axis label
  ylab("Mortality age") +
  scale_fill_manual(values=c("darkcyan", "cyan3")) +
  geom_hline(yintercept=40, linetype="dashed")
# dev.off()


#### FINAL: PLOT - SEX RATIO ####
# pdf("graphs/violin_sexratio.pdf")
ggplot(mapping=aes(x=sexratios$MC, y=sexratios$Values, fill = sexratios$GM), data = sexratios) +
  geom_split_violin() +
  theme_bw() +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  guides(fill=guide_legend(title="Grandmother effect")) +
  theme(axis.title.x = element_blank()) +   # Remove x-axis label
  ylab("Proportion female") +
  scale_fill_manual(values=c("darkcyan", "cyan3"))
# dev.off()

#### FINAL: PLOT - POPULATION SIZE ####
# pdf("graphs/violin_population.pdf")
ggplot(mapping=aes(x=whales$MC, y=whales$Values, fill = whales$GM), data = whales) +
  geom_split_violin() +
  theme_bw() +
  theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
  guides(fill=guide_legend(title="Grandmother effect")) +
  theme(axis.title.x = element_blank()) +   # Remove x-axis label
  ylab("Number of whales") +
  scale_fill_manual(values=c("darkcyan", "cyan3"))
# dev.off()


#### CHECK DATA ARE NORMALLY DISTRIBUTED ####
# PRR
shapiro.test(prr_fn$Values)
hist(prr_fn$Values, main="False/none", xlab = "Post-reproductive representation")
shapiro.test(prr_fm$Values)
hist(prr_fm$Values, main="False/male", xlab = "Post-reproductive representation")
shapiro.test(prr_ff$Values)
hist(prr_ff$Values, main="False/female", xlab = "Post-reproductive representation")

shapiro.test(prr_tn$Values)
hist(prr_tn$Values, main="True/none", xlab = "Post-reproductive representation")
shapiro.test(prr_tm$Values)
hist(prr_tm$Values, main="True/male", xlab = "Post-reproductive representation")
shapiro.test(prr_tf$Values)
hist(prr_tf$Values, main="True/female", xlab = "Post-reproductive representation")

## MORTALITY AGE
shapiro.test(final_deathage_fn)
hist(final_deathage_fn, main="False/none", xlab = "Mortality age")
shapiro.test(final_deathage_fm)
hist(final_deathage_fm, main="False/male", xlab = "Mortality age")
shapiro.test(final_deathage_ff)
hist(final_deathage_ff, main="False/female", xlab = "Mortality age")

shapiro.test(final_deathage_tn)
hist(final_deathage_tn, main="True/none", xlab = "Mortality age")
shapiro.test(final_deathage_tm)
hist(final_deathage_tm, main="True/male", xlab = "Mortality age")
shapiro.test(final_deathage_tf)
hist(final_deathage_tf, main="True/female", xlab = "Mortality age")

## PROPORTION POST-REPRODUCTIVE
shapiro.test(final_proportion_fn)
hist(final_proportion_fn, main="False/none", xlab = "Proportion post-reproductive")
shapiro.test(final_proportion_fm)
hist(final_proportion_fm, main="False/male", xlab = "Proportion post-reproductive")
shapiro.test(final_proportion_ff)
hist(final_proportion_ff, main="False/female", xlab = "Proportion post-reproductive")

shapiro.test(final_proportion_tn)
hist(final_proportion_tn, main="True/none", xlab = "Proportion post-reproductive")
shapiro.test(final_proportion_tm)
hist(final_proportion_tm, main="True/male", xlab = "Proportion post-reproductive")
shapiro.test(final_proportion_tf)
hist(final_proportion_tf, main="True/female", xlab = "Proportion post-reproductive")

## NUMBER OF WHALES
shapiro.test(final_whales_fn)
hist(final_whales_fn, main="False/none", xlab = "Number of whales")
shapiro.test(final_whales_fm)
hist(final_whales_fm, main="False/male", xlab = "Number of whales")
shapiro.test(final_whales_ff)
hist(final_whales_ff, main="False/female", xlab = "Number of whales")

shapiro.test(final_whales_tn)
hist(final_whales_tn, main="True/none", xlab = "Number of whales")
shapiro.test(final_whales_tm)
hist(final_whales_tm, main="True/male", xlab = "Number of whales")
shapiro.test(final_whales_tf)
hist(final_whales_tf, main="True/female", xlab = "Number of whales")

## SEX RATIO
shapiro.test(final_sexratio_fn)
hist(final_sexratio_fn, main="False/none", xlab = "Sex ratio")
shapiro.test(final_sexratio_fm)
hist(final_sexratio_fm, main="False/male", xlab = "Sex ratio")
shapiro.test(final_sexratio_ff)
hist(final_sexratio_ff, main="False/female", xlab = "Sex ratio")

shapiro.test(final_sexratio_tn)
hist(final_sexratio_tn, main="True/none", xlab = "Sex ratio")
shapiro.test(final_sexratio_tm)
hist(final_sexratio_tm, main="True/male", xlab = "Sex ratio")
shapiro.test(final_sexratio_tf)
hist(final_sexratio_tf, main="True/female", xlab = "Sex ratio")



#### FINAL: MODELS ####
## PRR ANOVA
## Check normality
par(mfrow=c(2,3))

# Two-way ANOVA
prr_model <- aov(prrs_new$Values ~ prrs_new$GM * prrs_new$MC)
summary(prr_model)

# Post-hoc to have a closer look
prr_posthoc <- TukeyHSD(prr_model)
prr_posthoc


## PRR T-TESTS
# Critical p = 0.05/6 = 0.0083 (Bonferroni)
t.test(fn_prr$Values, mu = 0) # 2.2e-16
t.test(fm_prr$Values, mu = 0) # 2.2e-16
t.test(ff_prr$Values, mu = 0) # 2.2e-16

t.test(tn_prr$Values, mu = 0) # 2.2e-16
t.test(tm_prr$Values, mu = 0) # 2.2e-16
t.test(tf_prr$Values, mu = 0) # 2.2e-16


## MORTALITY AGE

deathages_model <- aov(deathage$Values ~ deathage$GM * deathage$MC)
summary(deathages_model)

deathages_posthoc <- TukeyHSD(deathages_model)
deathages_posthoc


## PROPORTION POST-REPRODUCTIVE
proportion_model <- aov(proportions$Values ~ proportions$GM * proportions$MC)
summary(proportion_model)

proportion_posthoc <- TukeyHSD(proportion_model)
proportion_posthoc


## NUMBER OF WHALES
whales_model <- aov(whales$Values ~ whales$GM * whales$MC)
summary(whales_model)

whales_posthoc <- TukeyHSD(whales_model)
whales_posthoc


## SEX RATIOS
sexratio_model <- aov(sexratios$Values ~ sexratios$GM * sexratios$MC)
summary(sexratio_model)

sexratio_posthoc <- TukeyHSD(sexratio_model)
sexratio_model

## GROUPED MATE CHOICES
prr_mc <- rbind(tf_prr, tm_prr, tn_prr)
t.test(fn_prr$Values, prr_mc$Values)



#### TIME SERIES: VECTORS - PRR ####
## Create vectors (this takes a while)
ot_prr_ff <- mean_over_time(false_female, 2)
ot_prr_fm <- mean_over_time(false_male, 2)
ot_prr_fn <- mean_over_time(false_none, 2)
ot_prr_tn <- mean_over_time(true_none, 2)
ot_prr_tm <- mean_over_time(true_male, 2)
ot_prr_tf <- mean_over_time(true_female, 2)

ts_prr_fn <- ts(ot_prr_fn, frequency = 1)
ts_prr_fm <- ts(ot_prr_fm, frequency = 1)
ts_prr_ff <- ts(ot_prr_ff, frequency = 1)
ts_prr_tn <- ts(ot_prr_tn, frequency = 1)
ts_prr_tm <- ts(ot_prr_tm, frequency = 1)
ts_prr_tf <- ts(ot_prr_tf, frequency = 1)
beep(sound = 3)

#### TIME SERIES: VECTORS - NUMBER OF WHALES ####
ot_whales_ff <- mean_over_time(false_female, 10)
ot_whales_fm <- mean_over_time(false_male, 10)
ot_whales_fn <- mean_over_time(false_none, 10)
ot_whales_tf <- mean_over_time(true_female, 10)
ot_whales_tm <- mean_over_time(true_male, 10)
ot_whales_tn <- mean_over_time(true_none, 10)

ts_whales_fn <- ts(ot_whales_fn, frequency = 1)
ts_whales_fm <- ts(ot_whales_fm, frequency = 1)
ts_whales_ff <- ts(ot_whales_ff, frequency = 1)
ts_whales_tn <- ts(ot_whales_tn, frequency = 1)
ts_whales_tm <- ts(ot_whales_tm, frequency = 1)
ts_whales_tf <- ts(ot_whales_tf, frequency = 1)
beep(sound=3)

#### TIME SERIES: VECTORS - PROPORTION OF MENOPAUSAL WHALES ####
ot_menopop_fn <- (mean_over_time(false_none, 7) / ot_whales_fn)
ot_menopop_fm <- (mean_over_time(false_male, 7) / ot_whales_fm)
ot_menopop_ff <- (mean_over_time(false_female, 7) / ot_whales_ff)
ot_menopop_tn <- (mean_over_time(true_none, 7) / ot_whales_tn)
ot_menopop_tm <- (mean_over_time(true_male, 7) / ot_whales_tm)
ot_menopop_tf <- (mean_over_time(true_female, 7) / ot_whales_tf)

ts_menopop_fn <- ts(ot_menopop_fn, frequency = 1)
ts_menopop_fm <- ts(ot_menopop_fm, frequency = 1)
ts_menopop_ff <- ts(ot_menopop_ff, frequency = 1)
ts_menopop_tn <- ts(ot_menopop_tn, frequency = 1)
ts_menopop_tm <- ts(ot_menopop_tm, frequency = 1)
ts_menopop_tf <- ts(ot_menopop_tf, frequency = 1)
beep(sound = 3)

#### TIME SERIES: VECTORS - MORTALITY AGE ####
ot_deathage_fn <- mean_over_time(false_none, 4)
ot_deathage_fm <- mean_over_time(false_male, 4)
ot_deathage_ff <- mean_over_time(false_female, 4)
ot_deathage_tn <- mean_over_time(true_none, 4)
ot_deathage_tm <- mean_over_time(true_male, 4)
ot_deathage_tf <- mean_over_time(true_female, 4)

ts_deathage_fn <- ts(ot_deathage_fn, frequency = 1)
ts_deathage_fm <- ts(ot_deathage_fm, frequency = 1)
ts_deathage_ff <- ts(ot_deathage_ff, frequency = 1)
ts_deathage_tn <- ts(ot_deathage_tn, frequency = 1)
ts_deathage_tm <- ts(ot_deathage_tm, frequency = 1)
ts_deathage_tf <- ts(ot_deathage_tf, frequency = 1)

#### TIME SERIES: VECTORS - SEX RATIO ####
ot_sexratio_fn <- mean_over_time(false_none, 9)
ot_sexratio_fm <- mean_over_time(false_male, 9)
ot_sexratio_ff <- mean_over_time(false_female, 9)
ot_sexratio_tn <- mean_over_time(true_none, 9)
ot_sexratio_tm <- mean_over_time(true_male, 9)
ot_sexratio_tf <- mean_over_time(true_female, 9)

ts_sexratio_fn <- ts(ot_sexratio_fn, frequency = 1)
ts_sexratio_fm <- ts(ot_sexratio_fm, frequency = 1)
ts_sexratio_ff <- ts(ot_sexratio_ff, frequency = 1)
ts_sexratio_tn <- ts(ot_sexratio_tn, frequency = 1)
ts_sexratio_tm <- ts(ot_sexratio_tm, frequency = 1)
ts_sexratio_tf <- ts(ot_sexratio_tf, frequency = 1)
beep(sound = 3)




#### TIME SERIES: PLOTS - PRR ####
par(mfrow=c(1,1))
# pdf("graphs/line_prr_none.pdf")
plot.ts(ts_prr_fn, type="l", ylab = "Post-reproductive representation", ylim = c(0,0.4))
points(ot_prr_tn, col = "cyan4", pch=20,type = "l")
legend(5000,0.4, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,0.02, labels = "a)", cex = 2)
# dev.off()

# pdf("graphs/line_prr_male.pdf")
plot.ts(ts_prr_fm, type="l", ylab = "Post-reproductive representation", ylim = c(0,0.4))
points(ot_prr_tm, col = "cyan4", pch=20,type = "l")
legend(5000,0.4, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,0.02, labels = "b)", cex = 2)
# dev.off()

# pdf("graphs/line_prr_female.pdf")
plot.ts(ts_prr_ff, type="l", ylab = "Post-reproductive representation", ylim = c(0,0.4))
points(ot_prr_tf, col = "cyan4", pch=20,type = "l")
legend(5000,0.4, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,0.02, labels = "c)", cex = 2)
# dev.off()


#### TIME SERIES: PLOTS - POPULATION ####
# pdf("graphs/line_whales_none.pdf")
plot.ts(ts_whales_tn, type="l", ylab = "Number of whales", ylim = c(50, 400))
points(ot_whales_fn, col = "cyan4", pch=20,type = "l")
legend(5000,100, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,130, labels = "a)", cex = 2)
# dev.off()

# pdf("graphs/line_whales_male.pdf")
plot.ts(ts_whales_tm, type="l", ylab = "Number of whales", ylim = c(50, 400))
points(ot_whales_fm, col = "cyan4", pch=20,type = "l")
legend(5000,100, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,130, labels = "b)", cex = 2)
# dev.off()

# pdf("graphs/line_whales_female.pdf")
plot.ts(ts_whales_tf, type="l", ylab = "Number of whales", ylim = c(50, 400))
points(ot_whales_ff, col = "cyan4", pch=20,type = "l")
legend(5000,100, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,130, labels = "c)", cex = 2)
# dev.off()



#### TIME SERIES: PLOTS - PROPORTION MENOPAUSAL ####
# pdf("graphs/line_menopop_none.pdf")
plot.ts(ts_menopop_fn, type="l", ylab = "Proportion post-reproductive", ylim = c(0,0.13))
points(ot_menopop_tn, col = "cyan4", pch=20,type = "l")
legend(5000,0.13, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,0.105, labels = "a)", cex = 2)
# dev.off()

# pdf("graphs/line_menopop_male.pdf")
plot.ts(ts_menopop_fm, type="l", ylab = "Proportion post-reproductive", ylim = c(0,0.13))
points(ot_menopop_tm, col = "cyan4", pch=20,type = "l")
legend(5000,0.13, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,0.105, labels = "b)", cex = 2)
# dev.off()

# pdf("graphs/line_menopop_female.pdf")
plot.ts(ts_menopop_ff, type="l", ylab = "Proportion post-reproductive", ylim = c(0,0.13))
points(ot_menopop_tf, col = "cyan4", pch=20,type = "l")
legend(5000,0.13, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,0.105, labels = "c)", cex = 2)
# dev.off()

#### TIME SERIES: PLOTS - MORTALITY AGE ####
# pdf("graphs/line_deathage_none.pdf")
plot.ts(ts_deathage_fn, type="l", ylab = "Mortality age")
points(ot_deathage_tn, col = "cyan4", pch=20,type = "l")
legend(5000, 42.5, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,12, labels = "a)", cex = 2)
# dev.off()

# pdf("graphs/line_deathage_male.pdf")
plot.ts(ts_deathage_fm, type="l", ylab = "Mortality age")
points(ot_deathage_tm, col = "cyan4", pch=20,type = "l")
legend(5000, 5.25, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,12, labels = "b)", cex = 2)
# dev.off()

# pdf("graphs/line_deathage_female.pdf")
plot.ts(ts_deathage_ff, type="l", ylab = "Mortality age")
points(ot_deathage_tf, col = "cyan4", pch=20,type = "l")
legend(5000, 5.25, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,12, labels = "c)", cex = 2)
# dev.off()


#### TIME SERIES: PLOTS - SEX RATIO ####
# pdf("graphs/line_sexratio_none.pdf")
plot.ts(ts_sexratio_tn, type="l", ylab = "Proportion female", ylim = c(0.4,0.6))
points(ot_sexratio_fn, col = "cyan4", pch=20,type = "l")
legend(5000,0.6, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,0.41, labels = "a)", cex = 2)
# dev.off()

# pdf("graphs/line_sexratio_male.pdf")
plot.ts(ts_sexratio_tm, type="l", ylab = "Proportion female", ylim = c(0.4,0.6))
points(ot_sexratio_fm, col = "cyan4", pch=20,type = "l")
legend(5000,0.6, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,0.41, labels = "b)", cex = 2)
# dev.off()

# pdf("graphs/line_sexratio_female.pdf")
plot.ts(ts_sexratio_tf, type="l", ylab = "Proportion female", ylim = c(0.4,0.6))
points(ot_sexratio_ff, col = "cyan4", pch=20,type = "l")
legend(5000,0.6, legend = c("Without grandmother effect", "With grandmother effect"), lty = 1, lwd = 1.5, col = c("black", "cyan4"))
text(9800,0.41, labels = "c)", cex = 2)
# dev.off()



#### TIME SERIES: ADF TEST - PRR ####
adf_fn <- adf.test(ts_prr_fn, nlag = 1)
adf_fm <- adf.test(ts_prr_fm, nlag = 1)
adf_ff <- adf.test(ts_prr_ff, nlag = 1)

adf_tn <- adf.test(ts_prr_tn, nlag = 1)
adf_tm <- adf.test(ts_prr_tm, nlag = 1)
adf_tf <- adf.test(ts_prr_tf, nlag = 1)

#### TIME SERIES: ADF TEST - POPULATION ####
adf_fn <- adf.test(ts_whales_fn, nlag = 1)
adf_fm <- adf.test(ts_whales_fm, nlag = 1)
adf_ff <- adf.test(ts_whales_ff, nlag = 1)

adf_tn <- adf.test(ts_whales_tn, nlag = 1)
adf_tm <- adf.test(ts_whales_tm, nlag = 1)
adf_tf <- adf.test(ts_whales_tf, nlag = 1)

#### TIME SERIES: ADF TEST - PROPORTION MENOPAUSAL ####
adf_fn <- adf.test(ts_menopop_fn, nlag = 1)
adf_tn <- adf.test(ts_menopop_tn, nlag = 1)

adf_fm <- adf.test(ts_menopop_fm, nlag = 1)
adf_tm <- adf.test(ts_menopop_tm, nlag = 1)

adf_ff <- adf.test(ts_menopop_ff, nlag = 1)
adf_tf <- adf.test(ts_menopop_tf, nlag = 1)

#### TIME SERIES: ADF TEST - MORTALITY AGE ####
adf_fn <- adf.test(ts_deathage_fn, nlag = 1)
adf_tn <- adf.test(ts_deathage_tn, nlag = 1)

adf_fm <- adf.test(ts_deathage_fm, nlag = 1)
adf_tm <- adf.test(ts_deathage_tm, nlag = 1)

adf_ff <- adf.test(ts_deathage_ff, nlag = 1)
adf_tf <- adf.test(ts_deathage_tf, nlag = 1)

#### TIME SERIES: ADF TEST - SEX RATIO ####
adf_fn <- adf.test(ts_sexratio_fn, nlag = 1)
adf_tn <- adf.test(ts_sexratio_tn, nlag = 1)

adf_fm <- adf.test(ts_sexratio_fm, nlag = 1)
adf_tm <- adf.test(ts_sexratio_tm, nlag = 1)

adf_ff <- adf.test(ts_sexratio_ff, nlag = 1)
adf_tf <- adf.test(ts_sexratio_tf, nlag = 1)
